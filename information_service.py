import googlemaps
from main import app
from flask import request, jsonify

with open('notAPIkey.txt') as f:
    api_key = f.readline()
    gmaps = googlemaps.Client(api_key)


#Returns the place info related to the provided input
@app.route('/pinformation')
def get_place_information():
    req_data = request.get_json()
    address = get_address_from_coordinates(req_data['lat'], req_data['lon'])
    if address:
        place_id = get_place_from_address(address)
        if place_id:
            return jsonify(result=get_place_description(place_id))
        return jsonify(result=f"No information could be found at the address ({address})")
    return jsonify(result=f"No information could be found given the coordinates ({req_data['lat']}, {req_data['lon']})")

def get_address_from_coordinates(lat, lon):
    reverse_geocode_results = gmaps.reverse_geocode((lat, lon))
    if len(reverse_geocode_results) == 0:
        return None
    return reverse_geocode_results[0]['formatted_address']

def get_place_from_address(address):
    find_place_result = gmaps.find_place(address, "textquery", fields=['place_id'], location_bias=None, language=None)
    candidates = find_place_result['candidates']
    if len(candidates) == 0:
        return None
    return candidates[0]['place_id']

def get_place_description(place_id):
    details = gmaps.place(place_id)['result']
    rating = ''
    reviews = []
    if 'rating' in details:
        rating = details['rating']
    if 'reviews' in details:
        reviews = details['reviews']
    return { "rating": rating, "reviews": reviews }
