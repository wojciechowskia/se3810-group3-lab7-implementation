from flask import Flask
import googlemaps
app = Flask(__name__)

import information_service

if __name__ == "__main__":
    app.run(debug=True)
