# SE 3810 Group 3 Lab 7 Implementation
Authors: Sam Rousser, Andrew Wojciechowski, Hannah Rindfleisch

# Build Directions
1. You will need python and pip in your PATH environment variable.
2. Open a shell and navigate to the project directory.
3. Run `pip install -r requirements.txt`
4. To start the server run `python main.py`
5. To run the performance test run `pytest`