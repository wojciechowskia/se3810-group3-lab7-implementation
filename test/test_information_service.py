import pytest
import time
import json
import statistics
from main import app

@pytest.fixture
def locations():
    return [(41.4036, 2.1744),
    (27.1751, 78.0421),
    (43.7230, 10.3966),
    (40.4319, 116.5704),
    (48.8584, 2.2945),
    (-22.9519, -43.2105),
    (29.9753, 31.1376),
    (-90.0000, 45.0000),
    (43.8791, -103.4591),
    (45.0963, -94.4102),
    (-38.7338, 143.6972),
    (-18.2871, 147.6992),
    (38.8895, -77.0353),
    (-2.163106, -55.126648),
    (37.5665, 126.9780),
    (-17.9243, 25.8572),
    (-23.9884, 31.5547)]

@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as test_client:
        yield test_client

class TestInformationService:
    def test_get_place_information_performance(self, client, locations):
        times = []
        for location in locations:
            start = time.time()
            response = client.get('/pinformation', data=json.dumps({'lat': location[0], 'lon': location[1]}), content_type="application/json")
            end = time.time()

            assert response.status_code == 200

            times.append(round(end - start, 3))

        average = statistics.mean(times)

        print(f"Average Response Time: {average}")
        assert average < 5.0
        
